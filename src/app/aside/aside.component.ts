import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  @Input()
  selected: String;
  
  @Output()
  changeContent: EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  loadComponent(component: String){
    this.changeContent.emit(component);
  }

}
