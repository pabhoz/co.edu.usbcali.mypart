import { Component, OnInit } from '@angular/core';
import { ActivityService } from "services/activity";
import { Activity } from "models/activity";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  elements: any = [];
  //actividades: Activity[] = [];
  actividades: any = [];

  constructor(public activityService: ActivityService) { }

  ngOnInit() {
      //this.testService.getElements().then(data => this.elements = data);
      //this.testService.getIMC().then(data => console.log(data));
      this.activityService.get()
      .then(actividades =>{
        console.log(actividades);
        this.actividades = actividades;
      })
      .catch(error => console.log(error));
  }

  updateState(e){
   this.activityService.updateStatus(e.target.id)
   .subscribe(
      data => { 
        let response = data.json(); 
        if(response.error == 0){
          e.target.offsetParent.getElementsByTagName("span")[0].classList.add("done");
         }else{
           alert(response.msg);
          } 
      },
      err => console.log(err),
      () => console.log("Post Request Lista!")
    );
  }

}
