import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Activity } from "models/activity";
import { ActivityService } from "services/activity";

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.scss']
})
export class TareasComponent implements OnInit {

  constructor(public activityService:ActivityService) { }

  ngOnInit() {
  }

  addActivity(formulario: NgForm){
    
    let fileInput: any = document.getElementById("img");
    let files = fileInput.files[0];

    let imgPromise = this.getFileBlob(files);

    imgPromise.then(blob => {

      let tarea = new Activity(null,formulario.value.title,blob,1,1,null,null,0);
    
      this.activityService.post(tarea)
      .subscribe(
        data => { let response = data.json(); if(!response.error){ alert("Actividad Creada!"); }else{alert(response.msg)} },
        err => console.log(err),
        () => console.log("Post Request Lista!")
      );

    }).catch(e => console.log(e));
  }


  /*
  *
  * Método para convertir una URL de un archivo en un
  * blob
  * @author: pabhoz
  *
  */
  getFileBlob(file) {

      var reader = new FileReader();
      return new Promise(function(resolve, reject){

        reader.onload = (function(theFile) {
          return function(e) {
               resolve(e.target.result);
          };
        })(file);

        reader.readAsDataURL(file);

      });
 
  }

}
